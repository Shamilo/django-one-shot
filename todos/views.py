from django.shortcuts import render, get_list_or_404
from todos.models import TodoList

# Create your views here.
def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo = get_list_or_404(TodoList, id=id)
    context = {
        "todo_object": todo,
    }
    return render(request, "recipes/detail.html", context)
